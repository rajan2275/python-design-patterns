
1. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/singleton.py'>Singleton Pattern</a> <br>
2. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/lazy_instantiation_singleton.py'>Lazy instantiation in Singleton Pattern</a> <br>
3. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/singleton_with_metaclass.py'>Singleton with metaclass</a> <br>
4. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/factory.py'>Factory Pattern</a> <br>
5. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/abstract_factory.py'>Abstract factory Pattern</a> <br>
6. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/facade.py'>Facade Pattern</a> <br>
7. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/proxy.py'>Proxy Pattern</a> <br>
8. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/observer.py'>Observer Pattern</a> <br>
9. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/observer_example.py'>Observer Pattern in Publisher and Subscriber example</a> <br>
10. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/command.py'>Command Pattern</a> <br>
11. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/command_example.py'>Command Pattern Example</a> <br>
12. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/command_example_1.py'>Command Pattern Example_1</a> <br>
13. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/template.py'>Template Pattern</a> <br>
14. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/state.py'>State Pattern</a> <br>
15. <a href='https://github.com/rajan2275/Python-Design-Patterns/blob/master/MVC.py'>Model View Controller</a> <br>